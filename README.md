
```

⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢈⣌⣌⣬⣮⣎⣌⣌⢈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣌⣮⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣎⠈⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣈⣬⣾⣿⣿⣿⣿⣿⣿⣿⡷⡷⡷⣷⣿⣿⣿⣿⣿⢎⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⢀⣈⣬⣾⣿⣿⣿⣿⣿⣿⣿⡿⠳⠑⠀⠀⠀⠀⠀⣱⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⢀⣬⣿⣿⣿⣿⣿⣿⣿⡿⠷⠓⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠐⠑⠳⡷⣿⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⣀⣿⣿⣿⣿⣿⣿⠷⠓⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀█▀ █░█ █▀▀ █░░ ▀█▀ █▀▀
⠀⠀⠀⠀⣰⣿⣿⣿⣿⣿⠁⠀⠀⠀⠀⠀⠀⢈⣌⣮⣾⣿⣿⣿⣿⣯⣮⣌⢈⠀⠀⠀⠀⠀⠀⠀⠀⠀▄█ ▀▄▀ ██▄ █▄▄ ░█░ ██▄
⠀⠀⠀⠀⡰⣿⣿⣿⣿⣿⣏⢌⢈⢈⣌⣮⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣯⠌⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠱⣷⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡷⠳⠑⠑⠱⣷⣿⣿⣿⣿⣿⠌⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠑⠳⡷⣷⣿⣿⣿⣿⡿⡷⠳⠑⠀⠀⠀⠀⠀⠀⢀⣿⣿⣿⣿⣿⠏⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣈⣬⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠀⠀
⠀⠀⠀⢀⣿⣮⣌⢈⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣈⣬⣾⣿⣿⣿⣿⣿⣿⣿⠷⠁⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠰⣿⣿⣿⣿⣿⢏⠀⠀⠀⠀⠀⢈⣌⣾⣿⣿⣿⣿⣿⣿⣿⡿⠷⠓⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⡱⣿⣿⣿⣿⣿⣯⣮⣬⣮⣿⣿⣿⣿⣿⣿⣿⡿⠷⠓⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠰⡳⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡷⠓⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠑⠳⠳⡳⡷⠷⠳⠳⠑⠀⠀⠀⠀⠀⠀
```
---

## 📚 ¿Qué es?

Svelte es una tecnología que funciona más como un compilador de JS que como un framework/biblioteca al estilo *React/Vue/Angular/etc*. Svelte funciona en **tiempo de compilación**, generando Javascript que actualizará el dom de forma quirúrjica cuando sea necesario.

> "Traditional frameworks allow you to write declarative state-driven code, but there's a penalty: the browser must do extra work to convert those declarative structures into DOM operations, using techniques like virtual DOM diffing that eat into your frame budget and tax the garbage collector."



## 😎 Cool Stuff

### **Performance**
*Virtual DOM* *¿Es suficientemente rápido?*
<details>
  <summary>
  shouldComponentUpdate()  
  </summary>
  Aunque React solo actualiza los nodos DOM modificados, el re-renderizado aun lleva algo de tiempo. En muchos casos no es un problema, pero si la desaceleración es notable puedes acelerar el proceso anulando la función del ciclo de vida shouldComponentUpdate, el cual se ejecuta antes de que el proceso de re-renderizado comience 
  <a href="https://es.reactjs.org/docs/optimizing-performance.html#avoid-reconciliation">...</a>
</details>
<details>
  <summary>
  useMemo   
  </summary>
  useMemo solo volverá a calcular el valor memorizado cuando una de las dependencias haya cambiado. Esta optimización ayuda a evitar cálculos costosos en cada render. <a href="https://es.reactjs.org/docs/hooks-reference.html#usememo">...</a>
</details>

</br>  

*Algunos datos duros*  

![duration](./benchmark/1.png)
![startup](./benchmark/2.png)
![memory allocation](./benchmark/3.png)

### 🧐 **Accesibilidad**
 - Compile time warnings
   <ul class="svelte-1opg3ec"><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-accesskey">a11y-accesskey</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-aria-attributes">a11y-aria-attributes</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-autofocus">a11y-autofocus</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-distracting-elements">a11y-distracting-elements</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-hidden">a11y-hidden</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-img-redundant-alt">a11y-img-redundant-alt</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-invalid-attribute">a11y-invalid-attribute</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-label-has-associated-control">a11y-label-has-associated-control</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-media-has-caption">a11y-media-has-caption</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-misplaced-role">a11y-misplaced-role</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-misplaced-scope">a11y-misplaced-scope</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-missing-attribute">a11y-missing-attribute</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-missing-content">a11y-missing-content</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-mouse-events-have-key-events">a11y-mouse-events-have-key-events</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-no-redundant-roles">a11y-no-redundant-roles</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-positive-tabindex">a11y-positive-tabindex</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec active" href="https://svelte.dev/docs#accessibility-warnings-a11y-structure">a11y-structure</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-unknown-aria-attribute">a11y-unknown-aria-attribute</a> <ul class="svelte-1opg3ec"></ul> </li><li class="svelte-1opg3ec"><a class="subsection svelte-1opg3ec" href="https://svelte.dev/docs#accessibility-warnings-a11y-unknown-role" style="">a11y-unknown-role</a> <ul class="svelte-1opg3ec"></ul> </li></ul>

### 🧮 **Estilos**
- Scoped
- Preprocessor included
- Dead code
- Transitions translated to css

  
</br>

## ⌨ ¿Cómo empiezo?
> RollUp
```bash
$ npx degit sveltejs/template my-svelte-project
```

> Vite 

```bash
$ npm create vite@latest
```


## Referencias
- https://www.youtube.com/embed/AdNJ3fydeao
- https://krausest.github.io/js-framework-benchmark/2022/table_chrome_99.0.4844.51.html
- https://svelte.dev/blog/svelte-3-rethinking-reactivity
- https://svelte.dev/tutorial/basics
- https://massivepixel.io/blog/svelte-vs-react/


